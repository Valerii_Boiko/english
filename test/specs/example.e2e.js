const LandingPage = require('../pageobjects/landingPage');

describe('My landing page', () => {
    it('should display message', async () => {
        await LandingPage.open();

        let text = await LandingPage.logoText()
        await expect(text).toEqual('Welcome to BBC.com');
    });
});


