const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LandingPage extends Page {
    /**
     * define selectors using getter methods
     */
    get logoTextGetter() {
        return $('#page > section.module.module--header > h2 > span');
    }
    /**
     * a method to encapsule automation code to interact with the page
     */
    async logoText () {
        let content = await this.logoTextGetter.getText()
        return content;
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    open() {
        return super.open('https://www.bbc.com/');
    }
}

module.exports = new LandingPage();
